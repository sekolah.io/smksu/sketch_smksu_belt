/**********************************************************
 * Human Following Trolley - Belt System
 * by Sekolah Menengah Kebangsaan Saujana Utama
 * 
 * Team:
 *    1) Ameer
 *    2) Ghazi
 *    3) Khalish
 *    4) Ambreen
 *    
 * Manager:
 *    1) Hamidah Mukhtar
 *    2) 
 *    
 * Technical Coach:
 *    1) Sallehuddin Abdul Latif <salleh@hexabit.io>
 * 
 * Copyright (C) 2019 SMKSU
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <SPI.h>
#include <RH_ASK.h>

// Define all constant for the program

// The belt system requires:
// 1 HC-SR04 for sending echo signal
// 1 RF Transmitter to send timing signal for HC-SR04 in trolley system
// 1 RF Receiver to receive message from trolley system
// 1 buzzer to sound alarm based on message from trolley

// for HCSR04 (we just need to send sonar... no need to connect the echo pin)
#define sonar_Trigger 9 
#define sonar_Echo    8

// for radio Receiver
#define radio_In 11 // default RadioHead librasy initialise rx pin to 11

// for RF Transmitter
#define radio_Out 12 // default RadioHead library initialise tx pin to 12

// for buzzer
#define buzzer 13

// Signals
const char OK            = '0';
const char READY         = '1';
const char START         = '2';
const char TOO_FAR       = '3';
const char OBSTRUCTED    = '4';
const char RADIO_FAILURE = '5';

// radio communication driver
RH_ASK radio_driver;


void setup() {
  // put your setup code here, to run once:

  // set all pin type
  set_pin_mode();

  // initialize pin power
  pin_init();

  while (!radio_driver.init()) {
    //sound alarm
    sound_alarm(RADIO_FAILURE);
  }

  //sound buzzer to indicate the belt is ready
  sound_alarm(OK);
}

void loop() {
  // put your main code here, to run repeatedly:

  uint8_t receive_buf[1];
  uint8_t receive_buflen = sizeof(receive_buf);
  
  // Read message from trolley
  if (radio_driver.recv(receive_buf, &receive_buflen)) {
    String signal = String((char*)receive_buf);
    switch (signal[0]) {
      case OK:
        //
        break;
      case READY:
        // trolley ready to receive our next trigger signal
        // first send radio signal to mark start of trigger HC-SR04
        send_start_signal();
        
        // trigger the HC-SR04
        trigger_sonar();
        break;
      case TOO_FAR:
        sound_alarm(TOO_FAR);
        break;
      case OBSTRUCTED:
        sound_alarm(OBSTRUCTED);
        break;
      default:
        break;
    }
  } else {
    sound_alarm(RADIO_FAILURE);
  }  
}  

void set_pin_mode() {
  // HC-SR04 pins
  pinMode(sonar_Trigger, OUTPUT);
  pinMode(sonar_Echo, INPUT);

  // Radio pins
  pinMode(radio_In, INPUT);
  pinMode(radio_Out, OUTPUT);

  // Buzzer pin
  pinMode(buzzer, OUTPUT);
}

void pin_init() {
  clear_sonar_trigger();
  buzzer_OFF();
}

void clear_sonar_trigger() {
  digitalWrite(sonar_Trigger, LOW);
}

void send_start_signal() {
  String msg_signal = String(START);
  char *msg =  msg_signal.c_str();

  radio_driver.send((uint8_t *)msg, strlen(msg));
  radio_driver.waitPacketSent();
}

void trigger_sonar() {
  digitalWrite(sonar_Trigger, HIGH);
  delayMicroseconds(12);
  clear_sonar_trigger();
}

void buzzer_ON() {
  digitalWrite(buzzer, HIGH);
}

void buzzer_OFF() {
  digitalWrite(buzzer, LOW);
}

void sound_alarm(char reason) {
  switch (reason) {
    case OK:
      // we are ok
      buzzer_ON();
      delay(500);
      buzzer_OFF();
      break;
    case READY:
      // system is ready
      break;
    case TOO_FAR:
      // the trolley is futher then threshold
      buzzer_ON();
      delay(1000);
      buzzer_OFF();
      delay(500);
      buzzer_ON();
      delay(1000);
      buzzer_OFF();
      break;
    case OBSTRUCTED:
      // the trolley is obstructed
      buzzer_ON();
      delay(2500);
      buzzer_OFF();
      break;
    case RADIO_FAILURE:
      // radio initiation failure
      buzzer_ON();
      delay(500);
      buzzer_OFF();
      delay(200);
      buzzer_ON();
      delay(500);
      buzzer_OFF();
      delay(200);
      buzzer_ON();
      delay(500);
      buzzer_OFF();
      delay(200);
      break;
    default:
      //
      break;
  }
}
